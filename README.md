# Projet Spark Scala

Ce projet utilise Apache Spark avec Scala pour effectuer des analyses sur des données.

## Configuration requise

- Java 1.8
- Scala 2.12.18
- Apache Spark 3.5.1

## Installation

 Assurez-vous d'avoir Java 1.8 installé sur votre système.

## Utilisation

1. Clonez ce dépôt sur votre machine locale.
2. Compilez le projet en utilisant sbt : `sbt compile`.
3. Exécutez le programme en utilisant sbt : `sbt run`.

## Structure du projet

- `src/main/scala`: Contient le code source principal du projet.
- `build.sbt`: Fichier de configuration sbt pour définir les dépendances du projet et les paramètres de compilation.