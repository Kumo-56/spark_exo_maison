package utils

import org.apache.spark.rdd.RDD

object Functions {

    // 2. Créer un RDD qui contient le nombre d’occurrence de chaque mot dans chacun des
    //fichiers (le résultat doit avoir la forme d’une paire (clé, valeur)
    def countWords(lines: RDD[String]): RDD[(String, Int)] = {
      // Séparer chaque ligne en mots, compter les occurrences de chaque mot et les regrouper par clé
      val wordCounts = lines
        .flatMap(_.split("\\s+")) // Séparation des mots
        .map(word => (word, 1)) // Création d'une paire (mot, 1) pour chaque mot
        .reduceByKey(_ + _) // Agrégation des occurrences par mot

      wordCounts
    }

  // 6. Combiner les valeurs trouvées (résultat souhaité : (mot,v) avec v=nbre occ readme+nbre
  //occ changes).
  def sumByKey(rdd: RDD[(String, (Int, Int))]): RDD[(String, Int)] = {
    // Projection pour obtenir des paires (clé, somme des deux dernières valeurs)
    val summedRDD = rdd.map { case (key, (value1, value2)) => (key, value1 + value2) }

    // Faire la somme des valeurs pour chaque clé
    val resultRDD = summedRDD.reduceByKey(_ + _)

    resultRDD
  }

  // 2. Ecrire une fonction python get_label_type(label) qui permet d’afficher attack si la
  //valeur de la variable label est différente de normal. Sinon elle affiche normal
  def get_label_type(label: String): String = if (label == "normal.") "normal" else "attack"

}
