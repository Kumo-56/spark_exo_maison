import collect.RddDataframe
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import utils.Functions
import utils.Functions.get_label_type

object Main {
  def main(args: Array[String]): Unit = {

    // PARTIE 1
    val spark = SparkSession.builder()
      .appName("Exo Partie 1")
      .master("local")
      .getOrCreate()

    // Configuration du niveau de log
    spark.sparkContext.setLogLevel("ERROR")

    val filePath1 = "src/main/resources/CHANGES.txt"
    val filePath2 = "src/main/resources/README.md"

    val rdd1Option = RddDataframe.createRDDFromFile(spark.sparkContext, filePath1)
    val rdd2Option = RddDataframe.createRDDFromFile(spark.sparkContext, filePath2)
    val rdd1=RddDataframe.getRDD(rdd1Option)
    val rdd2=RddDataframe.getRDD(rdd2Option)

    // Appel de la méthode countWords pour compter les mots dans le RDD de lignes
    val wordCountsRdd1: RDD[(String, Int)] = Functions.countWords(rdd1)
    val wordCountsRdd2: RDD[(String, Int)] = Functions.countWords(rdd2)

    // Afficher le résultat
    //wordCountsRdd1.collect().foreach(println)

    // 3. Combiner les deux résultats dans un seul RDD (utiliser la jointure)
    val joinedRDD: RDD[(String, (Int, Int))] = wordCountsRdd1.join(wordCountsRdd2)


    // 4. Stocker le RDD trouvé en cache.
    joinedRDD.cache()

    // 5. Afficher la valeur du RDD résultant
    joinedRDD.collect().foreach(println)

    // Appel de la méthode sumByKey pour faire la somme des valeurs pour chaque clé
    val summedRDD: RDD[(String, Int)] = Functions.sumByKey(joinedRDD)

    // 7. Afficher les 5 premières lignes du résultat final
    val firstFiveLines: Array[(String, Int)] = summedRDD.take(5)
    println("5 premières lignes du RDD")
    firstFiveLines.foreach(println)

    // Ne pas oublier d'arrêter le SparkContext à la fin
    spark.stop()

    val spark2 = SparkSession.builder()
      .appName("Exo Partie 2")
      .master("local")
      .getOrCreate()


    // 1. Charger le fichier "kddcup.data_10_percent.gz" dans un RDD.
    val filePath3 = "src/main/resources/kddcup.data_10_percent.gz"

    val rdd3Option = RddDataframe.createRDDFromFile(spark2.sparkContext, filePath3)
    val rdd3 = RddDataframe.getRDD(rdd3Option)

    // 3. Utiliser la fonction map pour définir les types des différents champs
    val fields_rdd = rdd3.map(_.split(",")).map(p =>
      (p(0).toInt, p(1), p(2), p(3), p(4).toInt, p(5).toInt, get_label_type(p(41))))

    import spark2.implicits._

    // 4. Convertir le RDD en dataframe.
    val fields_df = fields_rdd.toDF("Duration", "protocol_type", "service", "flag", "src_bytes", "dst_bytes", "label")

    // 5. Créer la table interactions qui correspond au dataframe créer dans la question 4.
    fields_df.createOrReplaceTempView("interactions")

    // 6. Afficher les interactions du protocole tcp ayant une durée plus qu’une seconde et sans
    //transfert à partir de la destination (dst_bytes = 0).
    val tcp_interactions = spark2.sql("SELECT * FROM interactions WHERE protocol_type = 'tcp' AND Duration > 1 AND dst_bytes = 0")
    tcp_interactions.show()

    // 7. Afficher le nombre d’interaction par type de protocole
    val protocol_counts = spark2.sql("SELECT protocol_type, COUNT(*) AS count FROM interactions GROUP BY protocol_type")
    protocol_counts.show()

    // 8. Afficher le nombre de données par type attaque.
    val attack_counts = spark2.sql("SELECT label, COUNT(*) AS count FROM interactions GROUP BY label")
    attack_counts.show()


    spark2.stop()
  }
}