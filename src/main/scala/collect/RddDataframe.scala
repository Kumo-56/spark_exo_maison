package collect
import org.apache.spark.rdd.RDD
import org.apache.hadoop.mapred.InvalidInputException
import org.apache.spark.SparkContext

object RddDataframe {

  //1- Créer des RDD pour les deux fichiers
  def createRDDFromFile(spark: SparkContext,filePath: String): Option[RDD[String]] = {
    try {
      val rdd = spark.textFile(filePath)
      Some(rdd)
    } catch {
      case _: InvalidInputException =>
        println(s"Erreur : Fichier introuvable : $filePath")
        None
    }
  }


  def getRDD(optionRDD: Option[RDD[String]]): RDD[String] = {
    optionRDD match {
      case Some(rdd) => rdd
      case None => throw new IllegalArgumentException("RDD is not available")
    }
  }

}
